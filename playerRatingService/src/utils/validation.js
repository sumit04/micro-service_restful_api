const joi = require('@hapi/joi');

// validation using joi

// schema Id for validating Id
const schemaId = joi.object({
  id: joi
    .number()
    .integer()
    .required()
});

// schema for validating entries related to rating model
const schemaRating = joi
  .object()
  .keys({
    id: joi
      .number()
      .integer()
      .positive(),
    name: joi
      .string()
      .min(3)
      .max(20)
      .required(),
    rating: joi
      .number()
      .integer()
      .max(100)
      .required()
  })
  .required();

// schema for validating entries related to signUp of users
const schemaAuth = joi.object({
  id: joi.number().integer(),
  name: joi
    .string()
    .min(3)
    .max(20)
    .required(),
  email: joi
    .string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .required(),
  password: joi
    .string()
    .pattern(/^[a-zA-Z0-9]{5,20}$/)
    .required(),
  role: joi
    .string()
    .min(3)
    .max(6)
});

// schema for validating entries realted to login of users
const schemaLog = joi.object({
  email: joi
    .string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .required(),
  password: joi
    .string()
    .pattern(/^[a-zA-Z0-9]{5,20}$/)
    .required()
});
module.exports = {
  schemaId,
  schemaRating,
  schemaAuth,
  schemaLog
};
