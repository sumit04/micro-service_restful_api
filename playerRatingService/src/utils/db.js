const Sequelize = require('sequelize');
require('dotenv').config();

// conecton made to database using sequelize
const db = new Sequelize('postgres', 'postgres', process.env.PASS, {
  host: 'localhost',
  port: 5432,
  dialect: 'postgres',
  pool: {
    max: 60,
    min: 0,
    idle: 10000
  }
});

module.exports = db;
