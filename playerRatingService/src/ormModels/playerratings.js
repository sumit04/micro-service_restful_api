// creating model rating
// and exporitng it so that our api could use it
module.exports = (db, Sequelize) => {
  const microratings = db.define(
    'microratings',
    {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING,
        field: 'name'
      },
      rating: {
        type: Sequelize.INTEGER,
        field: 'rating'
        // primaryKey: false
      }
    },
    {
      // false cause we dont want to add timestamps
      timestamps: false
    }
  );

  // playerratings.associate = function(ormModels) {
  //   ormModels.playerratings.belongsTo(ormModels.playerdetails, {
  //     onDelete: 'cascade',
  //     hooks: true,
  //     foreignKey: 'Detail'
  //   });
  // };

  return microratings;
};
