// import playerrating model
const { microratings } = require('../ormModels');
const schema = require('../utils/validation');

const axios = require('axios');
// function to find player rating of specific id
function readRating(req, res, next) {
  const request = schema.schemaId.validate(req.params);
  if (request.error) {
    // res.status(400).send(request.error.details[0].message);
    next({ statusCode: 400, message: request.error.details[0].message });
  } else {
    microratings
      .findAll({
        where: {
          // raw: true,
          id: req.params.id
        }
      })
      .then(data => {
        res.status(200).send(data[0].dataValues);
      })
      .catch(() => {
        // res.status(404).send('Id not found');
        // next('Id not found');
        next({ statusCode: 404, message: 'Id not found' });
      });
  }
}

//function to call micro service from detail service

function detailedRating(req, res, next) {
  const request = schema.schemaId.validate(req.params);
  if (request.error) {
    // res.status(400).send(request.error.details[0].message);
    next({ statusCode: 400, message: request.error.details[0].message });
  } else {
    microratings
      .findAll({
        where: {
          // raw: true,
          id: req.params.id
        }
      })
      .then(data => {
        // res.status(200).send(data[0].dataValues);

        let detailedRatings = { rating: data[0].dataValues, detail: '' };
        axios
          .get('http://localhost:8080/detail/' + req.params.id)
          .then(response => {
            detailedRatings.detail = response.data;
            // console.log(detailedRating);
            res.json(detailedRatings);
          })
          .catch(err => {
            next({
              statusCode: 404,
              message: 'Error from service rating ' + err
            });
          });
      })
      .catch(() => {
        // res.status(404).send('Id not found');
        // next('Id not found');
        next({ statusCode: 404, message: 'Id not found' });
      });
  }
}

// function to read all player rating
function readAll(req, res, next) {
  microratings
    .findAll({
      raw: true
    })
    .then(data => res.status(200).send(data))
    .catch(() => {
      next({ statusCode: 404, message: 'Bad Request' });
    });
}

// function to insert player ratings in playerraating table
async function insertRatings(req, res, next) {
  const request = schema.schemaRating.validate(req.body);
  if (request.error) {
    // res.status(404).send(request.error.details[0].message);
    next({ statusCode: 404, message: request.error.details[0].message });
  } else {
    const id = await microratings.max('id');
    microratings
      .create({
        id: id + 1,
        name: req.body.name,
        rating: req.body.rating
      })
      .then(() => res.status(200).send(`Successfully inserted`))
      .catch(err => {
        // res.status(404).send(err.original.detail);
        // next(err.original.detail);
        next({ statusCode: 404, message: err.original.detail });
      });
  }
}

// function to delete raatings from payerratig table of sepecific id
function deleteRating(req, res, next) {
  const request = schema.schemaId.validate(req.params);

  if (request.error) {
    // res.status(400).send(request.error.details[0].message);
    // next(request.error.details[0].message);
    next({ statusCode: 400, message: request.error.details[0].message });
  } else {
    microratings
      .destroy({
        where: { id: req.params.id }
      })
      .then(data => {
        if (data) res.status(200).send('success');
        else throw new Error('id not found');
      })
      .catch(err => {
        // res.status(404).send(`delete failed with error: ${err}`);
        // next('Id not found');
        next({ statusCode: 404, message: 'Id Not Found' });
        // res.send(err);
      });
  }
}

// function to update raating of specific id
function updateRating(req, res, next) {
  try {
    const requestBody = schema.schemaRating.validate(req.body);
    const requestId = schema.schemaId.validate(req.params);
    if (requestBody.error) {
      throw requestBody.error;
    }
    if (requestId.error) throw requestId.error;

    microratings
      .update(
        {
          name: req.body.name,
          rating: req.body.rating
        },
        { where: { id: req.params.id } }
      )
      .then(data => {
        if (data[0] === 0) {
          // res.status(404).send('Id not found');
          // next('Id not found');
          next({ statusCode: 404, message: 'Id Not Found' });
        } else {
          res.status(201).send('updated Successfully');
        }
      })
      .catch(error => {
        throw error;
      });
  } catch (error) {
    // res.status(400).send(error.details[0].message);
    // next(error.details[0].message);
    next({ statusCode: 400, message: error.details[0].message });
  }
}

module.exports = {
  readAll,
  readRating,
  detailedRating,
  insertRatings,
  deleteRating,
  updateRating
};
