const express = require('express');
const rating = require('../controllers/rating');
// importing middle for verifing that user is authenicated or not
const { authUser, authAdmin } = require('../middleware/verifyToken');

const router = express.Router();

// requests for rating
// and connect with database
router.get('/:id', rating.readRating);

router.get('/', authUser, rating.readAll);

router.get('/:id/detail', authUser, rating.detailedRating);

router.post('/', authAdmin, rating.insertRatings);

router.delete('/:id', authAdmin, rating.deleteRating);

router.put('/:id', authAdmin, rating.updateRating);

module.exports = router;
