const express = require('express');
const auth = require('../controllers/auth');

const router = express.Router();

// routes request for login  signup and admin login
router.post('/signUp', auth.createUser);
router.post('/login', auth.loginUser);
router.post('/admin', auth.loginAdmin);

module.exports = router;

// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NCwiaWF0IjoxNTc2MjEyNTQ0fQ.EP_sJK0b-eOK10M0pbBJHhReNywUL3UxC2NaSsh6Jd4

// adin eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImFkbWluIiwiaWF0IjoxNTc2MjE5NzY0fQ.K2fMuHLjud0ewpWdSPNRNiuTQLpD8P03w6XR2JcGKBQ
