// npm run seedOrm
const csvToJson = require('csvtojson');

// function to convert csv to json
function ratings() {
  return csvToJson().fromFile('./src/seedOrm/playerRating.csv');
}
module.exports = {
  ratings
};
