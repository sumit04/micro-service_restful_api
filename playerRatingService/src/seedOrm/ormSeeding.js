const data = require('./csvTojson');
// importing tables{models} form ormModels
const { microratings, ratinguserroles } = require('../ormModels');

// function playerRating
// to seed rating of player to playerrating table
async function playerRating() {
  const ratingData = await data.ratings();
  // //model
  microratings
    .sync({ force: true })
    .then(() => {
      return microratings.bulkCreate(ratingData);
    })
    .catch(err => console.log(`error while creating table${err}`));
}

// function to insert data or seed in user table
async function userRoles() {
  ratinguserroles
    .sync({ force: false })
    .then(() => {
      return ratinguserroles.create({
        name: 'admin',
        email: 'admin@admin.com',
        password: 'admin',
        role: 'admin'
      });
    })
    .catch(err => console.log(`error while creating table${err}`));
}
// playerRating()
//   .then(() => playerDetail())
//   .then(() => userRoles())
//   .catch(err => console.log(`error while creating table${err}`));

async function seed() {
  try {
    await playerRating();
    await userRoles();
  } catch (err) {
    console.log(`error while creating table${err}`);
  }
}

seed();
