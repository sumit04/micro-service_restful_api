const jwt = require('jsonwebtoken');
// const { userroles } = require('../ormModels');

// middleware for verifing user
// auth# authentication
function authUser(req, res, next) {
  const token = req.header('user-token');
  if (!token) return res.status(401).send('access denied');

  try {
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);
    req.user = verified;
    // console.log(verified.id);
    next();
    // req.send(verified);
    // res.send(user);
  } catch (err) {
    res.status(400).send('invalid token');
  }
}

// midellware to verify the admin
function authAdmin(req, res, next) {
  const token = req.header('user-token');
  if (!token) return res.status(401).send('access denied');

  try {
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);
    // res.send(verified);
    if (verified.id === 'admin') {
      req.user = verified;

      next();
    } else throw new Error();
    // res.send(verified);
  } catch (err) {
    res.status(400).send('invalid token');
  }
}

// function authAdmin(req, res, next) {
//   const token = req.header('admin-token');
//   if (!token) return res.status(401).send('access denied');

//   try {
//     const verified = jwt.verify(token, process.env.ADMIN_SECRET);
//     req.user = verified;
//     next();
//   } catch (err) {
//     res.status(400).send('invalid token');
//   }
// }

module.exports = { authUser, authAdmin };
