// class ErrorHandler extends Error {
//   constructor(statusCode, message) {
//     super();
//     this.statusCode = statusCode;
//     this.message = message;
//   }
// }

const handleError = (err, req, res, next) => {
  res.status(err.statusCode).json({
    status: 'error',
    message: err.message
  });
  next(err.message);
};
module.exports = {
  handleError
};
