const express = require('express');
// imporing logger utility
const logger = require('./utils/logging');

// importng detail and rating and authntication routes
const detail = require('./route/details');
const auth = require('./route/auth');

// importing databse
const db = require('./utils/db');

// environmnt variable
require('dotenv').config();

const { handleError } = require('./middleware/errorMidware');

const app = express();

// logging all the info of the requests in apiInfoLog file
app.use(logger.log);
app.use(express.json());

//  routers details and rating
app.use('/', auth);
app.use('/detail', detail);

process.on('unhandledRejection', error => {
  // Will print "unhandledRejection err is not defined"
  console.log('unhandledRejection', error.message);
  // process.exit(0);
});

// all the invalid routes
app.use('*', (res, req, next) => {
  next({ statusCode: 404, message: 'Page Not Found' });
});

app.use(handleError);

// app.use((err, req, res, next) => {
//   handleError(err, res);
// });

// logging all the errors in the apiErrorLog file
app.use(logger.error);
// process.env.port have the port we defined in env
const port = process.env.PORT || 3000;
app.listen(port, error => {
  if (error) {
    console.log('error while starting server');
  } else {
    console.log(`hello sumit i am listing at ${port}`);
  }
});

// authenticating that db is connected
// db.authenticate('error', () => {
//   console.log('DB failed');
//   process.exit(0);
// });

db.authenticate()
  .then(() => console.log('database connected'))
  .catch(error => {
    console.log(`db failed${error}`);
    process.exit(0);
  });
