// library to hash passwords
const bcrypt = require('bcryptjs');

// library to genertae json web token
const jwt = require('jsonwebtoken');

// import table userroles from ormModels
const { detailuserroles } = require('../ormModels');

// impost schema for validation
const schema = require('../utils/validation');

// function for sign up for user, and create user in database
async function createUser(req, res, next) {
  // request={value,error} // it strores values and error relative to validation done
  const request = schema.schemaAuth.validate(req.body);

  //  Salts are in place to prevent someone from cracking passwords
  // A bcrypt cost of 6 means 64 rounds =2^6
  const salt = await bcrypt.genSalt(7);
  // making hash of the password
  const hashedPassword = await bcrypt.hash(req.body.password, salt);

  if (request.error) {
    // res.status(404).send(request.error.details[0].message);
    // next(request.error.details[0].message);
    next({ statusCode: 404, message: request.error.details[0].message });
  } else {
    detailuserroles
      .create({
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword,
        role: 'user'
      })
      .then(() => res.status(200).send({ succes: `successfully created user` }))
      .catch(err => {
        // res.status(404).send(err.original.detail);
        // next(err.original.detail);
        next({ statusCode: 404, message: err.original.detail });
      });
  }
}

// function for login of users
// it also assign user to role automatic
async function loginUser(req, res, next) {
  const request = schema.schemaLog.validate(req.body);

  if (request.error) {
    // res.status(404).send(request.error.details[0].message);
    // next(request.error.details[0].message);
    next({ statusCode: 404, message: request.error.details[0].message });
  }
  const user = await detailuserroles.findAll({
    where: {
      email: req.body.email
    }
  });
  // user will store [], if email is not found
  if (user.length === 0)
    // return res.status(400).send('Email not found');
    next({ statusCode: 400, message: "'Email not found'" });

  // decrypting and comparing with password
  const validPass = await bcrypt.compare(req.body.password, user[0].password);
  if (!validPass) next({ statusCode: 400, message: 'Invalid Password' });
  // res.status(400).send('Invalid Pass');

  // assigning json tokens with our own secret store in env
  const token = jwt.sign({ id: user[0].id }, process.env.TOKEN_SECRET);
  res.header('user-token', token).send(token);
}

// login for admin
async function loginAdmin(req, res, next) {
  const request = schema.schemaLog.validate(req.body);
  if (request.error) {
    // res.status(404).send(request.error.details[0].message);
    // next(request.error.details[0].message);
    next({ statusCode: 404, message: request.error.details[0].message });
  }
  // find in userRole for user of type admin
  const admin = await detailuserroles.findAll({
    where: {
      email: req.body.email,
      role: 'admin',
      password: req.body.password
    }
  });

  if (admin.length === 0)
    next({ statusCode: 401, message: 'unAuthorized access' });
  // return res.status(401).send('UnAuthorize access-e');

  //   const validPass = await bcrypt.compare(req.body.password, admin[0].password);
  //   if (!validPass) res.status(401).send('UnAuthorize access-p');

  // token of sign role
  const token = jwt.sign({ id: admin[0].role }, process.env.TOKEN_SECRET);
  res.header('admin-token', token).send(token);
}

module.exports = { createUser, loginUser, loginAdmin };
