// importing player details model
const { microdetails } = require('../ormModels');
const schema = require('../utils/validation');
const axios = require('axios');

// function to read details of players of specific id
function readDetails(req, res, next) {
  const request = schema.schemaId.validate(req.params);
  if (request.error) {
    // res.status(400).send(request.error.details[0].message);
    // next(request.error.details[0].message);
    next({ statusCode: 400, message: request.error.details[0].message });
  } else {
    microdetails
      .findAll({
        where: {
          // raw: true,
          id: req.params.id
        }
      })
      .then(data => {
        res.status(200).send(data[0].dataValues);
      })
      .catch(() => {
        // res.status(404).send('Id not found');
        // next('Id not found');
        next({ statusCode: 404, message: 'Id not found' });
      });
  }
}

//microservice
// it will call rating service
function readRating(req, res, next) {
  const request = schema.schemaId.validate(req.params);
  if (request.error) {
    // res.status(400).send(request.error.details[0].message);
    // next(request.error.details[0].message);
    next({ statusCode: 400, message: request.error.details[0].message });
  } else {
    microdetails
      .findAll({
        where: {
          // raw: true,
          id: req.params.id
        }
      })
      .then(data => {
        // res.status(200).send(data[0].dataValues);
        let detailedRating = { rating: '', detail: data[0].dataValues };
        axios
          .get('http://localhost:3000/rating/' + req.params.id)
          .then(response => {
            detailedRating.rating = response.data;
            // console.log(detailedRating);
            res.json(detailedRating);
          })
          .catch(err => {
            next({
              statusCode: 404,
              message: 'Error from service rating ' + err
            });
          });
      })
      .catch(() => {
        // res.status(404).send('Id not found');
        // next('Id not found');
        next({ statusCode: 404, message: 'Id not found' });
      });
  }
}

// function to readd all users details
function readAll(req, res, next) {
  microdetails
    .findAll({
      raw: true
    })
    .then(data => res.status(200).send(data))
    .catch(err => {
      res.status(400).send(err);
      next('Bad Request');
    });
}

// function to insert details of player in detail table
async function insertDetails(req, res, next) {
  const request = schema.schemaDetail.validate(req.body);
  if (request.error) {
    // res.status(400).send(request.error.details[0].message);
    // next(request.error.details[0].message);
    next({ statusCode: 404, message: request.error.details[0].message });
  } else {
    const id = await playerdetails.max('id');
    res.send(id);
    microdetails
      .create({
        id: id,
        nation: req.body.nation,
        age: req.body.age,
        club: req.body.club,
        kit: req.body.kit,
        position: req.body.position
      })
      .then(() => res.status(200).send(`Successfully inserted`))
      .catch(err => {
        // res.status(404).send(err.original.detail);
        // next(err.original.detail);
        next({ statusCode: 404, message: err.original.detail });
      });
  }
}

// function to delete details from playedetails table
function deleteDetails(req, res, next) {
  const request = schema.schemaId.validate(req.params);

  if (request.error) {
    // res.status(400).send(request.error.details[0].message);
    // next(request.error.details[0].message);
    next({ statusCode: 400, message: request.error.details[0].message });
  } else {
    microdetails
      .destroy({
        where: { id: req.params.id }
      })
      .then(data => {
        if (data) res.status(200).send('success');
        else throw new Error('id not found');
      })
      .catch(err => {
        // res.status(404).send(`delete failed with error: ${err}`);
        // next('Id not found');
        next({ statusCode: 404, message: 'Id Not Found' });
      });
  }
}

// function to update details of player in playerdetail tables
function updateDetails(req, res, next) {
  try {
    const requestBody = schema.schemaDetail.validate(req.body);
    const requestId = schema.schemaId.validate(req.params);
    if (requestBody.error) {
      throw requestBody.error;
    }
    if (requestId.error) throw requestId.error;
    microdetails
      .update(
        {
          nation: req.body.nation,
          age: req.body.age,
          club: req.body.club,
          kit: req.body.kit,
          position: req.body.position
        },
        { where: { id: req.params.id } }
      )
      .then(data => {
        if (data[0] === 0) {
          // res.status(404).send('Id not found');
          // next('Id not found');
          next({ statusCode: 404, message: 'Id Not Found' });
        } else {
          res.status(201).send('updated Successfully');
        }
      })
      .catch(error => {
        throw error;
      });
  } catch (error) {
    // res.status(400).send(error.details[0].message);
    // next(error.details[0].message);
    next({ statusCode: 400, message: error.details[0].message });
  }
}

module.exports = {
  readAll,
  readRating,
  readDetails,
  insertDetails,
  deleteDetails,
  updateDetails
};
