const data = require('./csvTojson');
// importing tables{models} form ormModels
const { microdetails, detailuserroles } = require('../ormModels');

// function playerDetails
// to seed details of player in playerdetail table
async function playerDetail() {
  const playerdetail = await data.details();
  // model2
  microdetails
    .sync({ force: true })
    .then(() => {
      return microdetails.bulkCreate(playerdetail);
    })
    .catch(err => console.log(`error while creating table${err}`));
}

// function to insert data or seed in user table
async function userRoles() {
  detailuserroles
    .sync({ force: true })
    .then(() => {
      return detailuserroles.create({
        name: 'admin',
        email: 'admin@admin.com',
        password: 'admin',
        role: 'admin'
      });
    })
    .catch(err => console.log(`error while creating table${err}`));
}
// playerRating()
//   .then(() => playerDetail())
//   .then(() => userRoles())
//   .catch(err => console.log(`error while creating table${err}`));

async function seed() {
  try {
    await playerDetail();
    await userRoles();
  } catch (err) {
    console.log(`error while creating table${err}`);
  }
}

seed();
