// npm run seedOrm
const csvToJson = require('csvtojson');

// function to convert csv to json
function details() {
  return csvToJson().fromFile('./src/seedOrm/playerDetail.csv');
}

module.exports = {
  details
};
