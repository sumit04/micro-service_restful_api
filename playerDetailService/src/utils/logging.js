const winston = require('winston');
const expressWinston = require('express-winston');

// using express winston and winston to logg info and errors
module.exports = {
  log: expressWinston.logger({
    transports: [
      new winston.transports.File({
        filename: 'apiInfoLog.log'
        // level: 'info'
      })
    ],
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.json()
    )
  }),
  error: expressWinston.errorLogger({
    transports: [
      new winston.transports.File({
        filename: 'apiError.log'
        // level: 'error'
      })
    ],
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.json()
    )
  })
};
// module.exports = { loggerLog, loggerError };
