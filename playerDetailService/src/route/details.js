const express = require('express');
const details = require('../controllers/details');
// importing middle for verifing that user is authenicated or not
const { authUser, authAdmin } = require('../middleware/verifyToken');

const router = express.Router();

// request for getting,updating,deleting fom readdetail models
router.get('/:id', details.readDetails);

router.get('/', authUser, details.readAll);

router.get('/:id/rating', authUser, details.readRating);

router.post('/', authAdmin, details.insertDetails);

router.delete('/:id', authAdmin, details.deleteDetails);

router.put('/:id', authAdmin, details.updateDetails);

module.exports = router;
