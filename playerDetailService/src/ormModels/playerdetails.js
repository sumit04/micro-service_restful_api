// creating model player details
// and exporitng it so that our api could use it
module.exports = (db, Sequelize) => {
  const microdetails = db.define(
    'microdetails',
    {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      nation: {
        type: Sequelize.STRING,
        field: 'nation'
      },
      age: {
        type: Sequelize.INTEGER,
        field: 'age'
        // primaryKey: false
      },
      club: {
        type: Sequelize.STRING,
        field: 'club'
      },
      kit: {
        type: Sequelize.INTEGER,
        filed: 'kit'
      },
      position: {
        type: Sequelize.STRING,
        filed: 'position'
      }
    },
    {
      // dont add time coloumns in table
      timestamps: false
    }
  );

  // // association for cascadind the model with rating to detail
  // // if rating is deleted then detail will also get deleted
  // playerdetails.associate = function(ormModels) {
  //   ormModels.playerdetails.belongsTo(ormModels.playerratings, {
  //     onDelete: 'cascade',
  //     hooks: true,
  //     foreignKey: { filed: 'ratingId', autoIncrement: true }
  //   });
  // };

  return microdetails;
};
