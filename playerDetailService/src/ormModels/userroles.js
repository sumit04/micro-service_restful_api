// creating model for users who will use the api
// and exporitng it so that our api could use it
module.exports = (db, Sequelize) => {
  const detailuserroles = db.define(
    'userroles',
    {
      // id: {
      //   type: Sequelize.INTEGER,
      //   field: 'id',
      //   autoIncrement: true,
      //   primaryKey: true
      // },
      name: {
        type: Sequelize.STRING,
        field: 'name'
      },
      email: {
        type: Sequelize.STRING,
        field: 'email',
        unique: true,
        primaryKey: true
      },
      password: {
        type: Sequelize.STRING,
        field: 'password'
        // primaryKey: false
      },
      role: { type: Sequelize.STRING, field: 'role' }
    },
    {
      // timestamps to remove timespams from table
      timestamps: false
    }
  );
  //   admin.associate = ormModels => {
  //     admin.belongsTo(ormModels.user);
  //   };

  return detailuserroles;
};
