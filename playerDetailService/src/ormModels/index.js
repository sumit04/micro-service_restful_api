const Sequelize = require('sequelize');
// importing database that's being connected to server
const db = require('../utils/db');

// making a model object stores models that we have created
const models = {
  microdetails: db.import('./playerdetails'),
  detailuserroles: db.import('./userroles')
};

Object.keys(models).forEach(modelName => {
  if (models[modelName].associate) {
    models[modelName].associate(models);
  }
});

models.db = db;
models.Sequelize = Sequelize;

module.exports = models;
