# EXAMPLE PROJECT FOR MICROSERVICE_RESTful_APIs

```
Example project for:

Micro-service APIs
Node 
Express
Postgres
Sequelize-Orm
Express-winston for logging.
Hapi/Joi for validation.
JWT tokens for authentication.
```
```
Each service is independent from each other
Each could be define as-
An Backend API for a web app.
An RESTful API that uses HTTP requests to GET, PUT, POST and DELETE data.
Works on Express framework.
```

# Features
```
Micro-Service
Axiom is used as protocol for sub-system communication
```

```
EXPRESS
```
```
REST API
```
```
Sequelize(ORM)
```
```
DATABASE - PostGres
```
```
Logging - Express-Winston
```
```
Validation - Hapi/Joi
```
```
Authentication - Jwt and bcrypt
```


## Requirements and Prerequisites
```
node & npm
```
```
postgres
```
```
git
```

## Installing

To get the Node server running locally:
```
Clone the branch using below command

git clone https://gitlab.com/sumit04/micro-service_restful_api.git
```
```
important:
As both services are independent from each other:
after cloning you have to (run npm install) for both the service individually.
```
```
npm install for both  to install all required dependencies
```
```
Install postgress database
```
```
FOR SEEDING service1- RUN npm run ratingseed
FOR SEEDING service2- RUN npm run detailseed
```
```
set .env variable for port and password of postgres
set .env for secretCode for JWT tokens
```
```
npm detailstart to start detail service
npm ratingstart to start rating service
```
```
Postman
Install Postman to interact with REST API
```
```
Create a message with:
URL:

http://localhost:PORT/signUP
Method: POST
Body: raw + JSON (application/json)
Body Content: { "name":"name", "email": "email@eamil.com","password":"password" }

http://localhost:PORT/login
Method: POST
Body: raw + JSON (application/json)
Body Content: { "email": "email@eamil.com","password":"password" }

http://localhost:PORT/rating
Method: GET

http://localhost:PORT/detal
Method: GET
```



## Code Overview for working of microservice


```
This is router of DETAIL service

router.get('/:id/rating', authUser, details.readRating);

```

```
This function is of DETAIL service
It will commnuciate with RATING service
to provide player detail and rating

function readRating(req, res, next) {
  const request = schema.schemaId.validate(req.params);
  if (request.error) {
    // res.status(400).send(request.error.details[0].message);
    // next(request.error.details[0].message);
    next({ statusCode: 400, message: request.error.details[0].message });
  } else {
    microdetails
      .findAll({
        where: {
          // raw: true,
          id: req.params.id
        }
      })
      .then(data => {
        // res.status(200).send(data[0].dataValues);
        let detailedRating = { rating: '', detail: data[0].dataValues };
        axios
          .get('http://localhost:3000/rating/' + req.params.id)        // this will call for the rating service and request for rating of player of ID we passed//
          .then(response => {
            detailedRating.rating = response.data;
            // console.log(detailedRating);
            res.json(detailedRating);
          })
          .catch(err => {
            next({
              statusCode: 404,
              message: 'Error from service rating ' + err
            });
          });
      })
      .catch(() => {
        // res.status(404).send('Id not found');
        // next('Id not found');
        next({ statusCode: 404, message: 'Id not found' });
      });
  }
}

```
```
USE: http://localhost:PORT/detail/ID/rating
     Method: GET

LET ID=1

OUTPUT:
{
    "rating": {
        "id": 1,
        "name": "Cristiano Ronaldo",
        "rating": 94
    },
    "detail": {
        "id": 1,
        "nation": "Portugal",
        "age": 32,
        "club": "Real Madrid",
        "kit": 7,
        "position": "LW"
    }
}
```
     

#### Dependencies

```

    AXIOM:  Used as protocol for sub-system communication
```
 ```
    EXPRESS: The server for handling and routing HTTP requests
```
```
    POSTGRES: Database
```
```
    SEQUELIZE: It is a promise-based Node.js ORM
```
```
    JSONwebTOKEN and BcryptJS : For authentication and authorization.
```
```
    HAPI/JOI: FOR Validation
```
```
    Express-Winston: For logging the info and errors
```

#### Application Structure

```
src/index.js- The entry point to our application. [ npm start ]
```
```
src/seedData- It constains the script for seeding the data to database. [npm run seedOrm]
```
```
src/routes/- This folder contains the route for our API
```
```
src/models || src/ormModels- This folder contains the schema definitions for our models
```
```
src/controler-  This folder contains configuration  and defination for all routes of our APi
```

#### Error Handling

```
In src/middleware, we have define a error handling middleware
```

#### Authentication
```
Requests are authenticated using the Authorization header with a valid JWT.
We define express middlewares in src/middleware that can be used to authenticate requests.
```
    
    
## Authors

* **Sumit Aswal**

